package bossacorp.virtualsceneperformer.utils.configuration;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

import bossacorp.virtualsceneperformer.utils.GlobalSettings;
import bossacorp.virtualsceneperformer.utils.GlobalSettings.ControllerType;

@Root(name="controller")
public class ControllerConfiguration {

	@Attribute
	private String type;
	
	@Attribute
	private String port;
	
	public String getType(){
		return type;
	}
	
	public String getControllerName(){
		return port;
	}
	
	public ControllerType getInterfaceType(){
		if(type.equals("MIDI")) return ControllerType.MIDI;
				
		return ControllerType.NONE;
	}
	
	public String getMidiName(){
		if(port.equals("UM-ONE")) return GlobalSettings.UM_ONE;
		if(port.equals("USB MIDI Interface")) return GlobalSettings.Roland;
		
		return "";
	}

}
