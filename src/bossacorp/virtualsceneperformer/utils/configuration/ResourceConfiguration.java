package bossacorp.virtualsceneperformer.utils.configuration;


import java.util.List;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

@Root(name="resource")
public class ResourceConfiguration {

	@Attribute
	private String type;

	@Attribute
	private String name;
	
	@Element(name="srcLocation")
	private String location;
	
	@ElementList(inline = true, name="controller")
	private List<ControllerConfiguration> controller;
	
	public String getType(){
		return type;
	}

	public String getName(){
		return name;
	}
	
	public String getLocation(){
		return location;
	}

	public List<ControllerConfiguration> getFxConfiguration(){
		return controller;
	}
	
}
