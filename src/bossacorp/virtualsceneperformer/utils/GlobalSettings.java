package bossacorp.virtualsceneperformer.utils;

public class GlobalSettings {

	public static enum ProjectionType{KEYBOARD, AUTO};
	
	public static enum ControllerType{MIDI, CAMERAS, OSC, NONE};
	
	public static String Roland = "USB MIDI Interface";
	public static String UM_ONE = "UM_ONE";

	public static String DefaultScriptFile = "script.xml";
}
