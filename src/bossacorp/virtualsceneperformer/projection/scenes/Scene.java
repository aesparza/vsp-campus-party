package bossacorp.virtualsceneperformer.projection.scenes;

import java.util.ArrayList;
import java.util.logging.Level;

import bossacorp.virtualsceneperformer.fx.basics.Sketch;
import bossacorp.virtualsceneperformer.utils.GlobalSettings.ControllerType;
import bossacorp.virtualsceneperformer.utils.VSPSystemLogger;
import bossacorp.virtualsceneperformer.utils.configuration.SceneConfiguration;
import processing.core.PApplet;

public class Scene implements IScene {

	private SceneConfiguration sceneConfiguration;
	private ControllerType controller;
	String image;
	String deviceName;
	PApplet parent;
	Sketch sketch;
	ArrayList<String>devices;
	
	public Scene(PApplet parent, SceneConfiguration config){
		sceneConfiguration = config;
		this.parent = parent;
		setUpScene();
	}

	@Override
	public void setUpScene() {
		devices = new ArrayList<String>();
		for(int i=0; i < sceneConfiguration.getResourceList().size(); i++){
								    
			image = sceneConfiguration.getResourceList().get(i).getLocation();
			
			for(int j = 0; j < sceneConfiguration.getResourceList().get(i).getFxConfiguration().size(); j++){
				controller = sceneConfiguration.getResourceList().get(i).getFxConfiguration().get(j).getInterfaceType();
				deviceName = sceneConfiguration.getResourceList().get(i).getFxConfiguration().get(j).getControllerName();
				devices.add(deviceName);
				
				VSPSystemLogger.getLogger().log(Level.INFO, "Image: " + image + ", Controller: " + controller + ", device: " + deviceName);
			}
			
			
			
			sketch = new Sketch(parent, image, devices);
			
		}

		VSPSystemLogger.getLogger().log(Level.INFO,"Generic Scene Set Up done");
	}

	@Override
	public void drawScene() {
		sketch.draw();
	}
	
	@Override
	public SceneConfiguration getSceneConfiguration() {
		return sceneConfiguration;
	}

}
