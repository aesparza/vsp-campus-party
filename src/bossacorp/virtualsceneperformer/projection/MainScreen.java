package bossacorp.virtualsceneperformer.projection;

import java.util.logging.Level;

import bossacorp.virtualsceneperformer.projection.scenes.SceneManager;
import bossacorp.virtualsceneperformer.utils.VSPSystemLogger;
import processing.core.PApplet;

public class MainScreen extends PApplet{
	
	SceneManager sceneManager;
	
	public static void main(String[] args) {
		VSPSystemLogger.getLogger().log(Level.INFO,"Bienvenido a VSP - Virtual Scene Performer");
        PApplet.main("bossacorp.virtualsceneperformer.projection.MainScreen");
    }
	
	public void settings(){
		size(1000,500, P3D);//Por ahorita estoy dejando este tama�o para poder ver el log 
		//fullScreen();
	}
	
	public void setup() {
		  noStroke();  
		  sceneManager = new SceneManager(this);
	}
	
	public void draw() { 
		background(0);
		sceneManager.getCurrentScene().drawScene();
	}	
}
