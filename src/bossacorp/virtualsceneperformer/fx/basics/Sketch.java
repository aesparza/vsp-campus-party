package bossacorp.virtualsceneperformer.fx.basics;

import java.util.ArrayList;
import java.util.logging.Level;

import bossacorp.virtualsceneperformer.utils.VSPSystemLogger;
import processing.core.PApplet;
import processing.core.PConstants;
import processing.core.PImage;
import rwmidi.*;

public class Sketch {

	PApplet parent;
	PImage image;
	String deviceName;
	ArrayList<String> devices = new ArrayList<String>();
	MidiInput input;
	
	public Sketch(PApplet parent, String img, ArrayList<String> devices){
		this.parent = parent;
		this.devices = devices;
		
		if(!devices.contains("NONE"))
			initMIDI();
		
		this.parent.fill(255);
		this.parent.noStroke();
		
	}
	
	public void draw(){
		
		  parent.lights();
		  parent.translate(parent.width/2,parent.height/2);
		  parent.rotateX(-PApplet.atan(PApplet.sin(PConstants.PI/4)));
		  parent.rotateY(PConstants.PI/4+PConstants.TWO_PI*parent.frameCount/60/20);
		  float radius = 20;
		  float angle = -PConstants.TWO_PI*parent.frameCount/60/60;
		  float speed = 10;
		  float deltaAngle;
		  for(int i=0;i<512;i++){
			 parent.pushMatrix();
		    deltaAngle = speed/radius;
		    angle += deltaAngle;
		    radius += 20/PConstants.TWO_PI*deltaAngle;
		    parent.rotateX(PConstants.PI/64*PApplet.cos(PConstants.TWO_PI*i/512*12-PConstants.TWO_PI*parent.frameCount/60/2));
		    parent.rotateY(PConstants.PI/64*PApplet.cos(PConstants.TWO_PI*i/512*12-PConstants.TWO_PI*parent.frameCount/60/2.1f));
		    parent.translate(radius*PApplet.cos(angle),radius*PApplet.sin(angle));
		    parent.rotateZ(angle);
		    parent.fill(
		    128+127*PApplet.cos(PConstants.TWO_PI*i/512*32-PConstants.TWO_PI*parent.frameCount/60/-1.9f),
		    128+127*PApplet.cos(PConstants.TWO_PI*i/512*42-PConstants.TWO_PI*parent.frameCount/60/2.1f),
		    128+127*PApplet.cos(PConstants.TWO_PI*i/512*22-PConstants.TWO_PI*parent.frameCount/60/-2));
		    parent.rotateX(PConstants.PI/8*PApplet.cos(PConstants.TWO_PI*i/512*9-PConstants.TWO_PI*parent.frameCount/60/3));
		    parent.box(16+4*PApplet.cos(PConstants.TWO_PI*i/512*16-PConstants.TWO_PI*parent.frameCount/60),8,8);
		    parent.popMatrix();
		  }

		 
	}
	
	public void noteOnReceived(Note note){
		VSPSystemLogger.getLogger().log(Level.INFO,"Midi Device Asignado a -> : " + note.getPitch() );
	}
		
	public void initMIDI(){
		for(int i=0; i < RWMidi.getInputDevices().length-1; i++){
						
			if(RWMidi.getInputDeviceNames()[i].contains(devices.get(i)))
			{				
				input = RWMidi.getInputDevices()[i].createInput(this);
				VSPSystemLogger.getLogger().log(Level.INFO,"Midi Device Asignado a -> : " + input.getName() );
			}
		}	
	}
	
	
	
	
}
